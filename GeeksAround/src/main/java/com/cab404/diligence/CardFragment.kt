package com.cab404.diligence

import android.graphics.PointF
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.cab404.libtcpchat.TcpChat
import kotlinx.android.synthetic.main.fragment_card.*
import kotlin.math.sign

class CardFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_card, container, false)

    var colors = mutableListOf<Int>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        TcpChat(context!!).getAddressList()

        vTopLayer.setOnTouchListener(object : View.OnTouchListener {

            var start: PointF? = null
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                val current_point = PointF(event.rawX, event.rawY)

                fun onSwipe(side: Int) {
                    val prev = vTopLayer.background
                    ViewCompat.setBackground(vTopLayer, vBottomLayer.background)
                    ViewCompat.setBackground(vBottomLayer, prev)
                    vTopLayer.translationX = 0f
                    vTopLayer.translationY = 0f
                    vTopLayer.rotation = 0f
                    if (side == 1)
                        Toast.makeText(context, "You liked this color!", Toast.LENGTH_SHORT).show()
                    else
                        Toast.makeText(context, "You disliked this color :(", Toast.LENGTH_SHORT).show()
                }

                fun onTap() {

                }

                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        start = current_point
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val start = start!!
                        val vec = PointF(current_point.x - start.x, current_point.y - start.y)
                        v.rotation = (vec.x / v.width) * 40
                        v.translationX = vec.x
                        v.translationY = vec.y
                    }
                    MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                        // if we are letting view go
                        val start = start!!
                        val vec = PointF(current_point.x - start.x, current_point.y - start.y)
                        ViewCompat.animate(v).cancel()
                        if (Math.abs(vec.x) > v.width / 2f) {
                            // If swipe successful

                            // getting target side
                            val side = sign(vec.x).toInt()
                            ViewCompat.animate(v)
                                    .setListener(object : ViewPropertyAnimatorListenerAdapter() {
                                        override fun onAnimationEnd(v: View?) {
                                            onSwipe(side)
                                        }
                                    })
                                    .translationX(v.width.toFloat() * 2 * side)
                                    .start()
                        } else {
                            // If swipe failed
                            // animating things back to where they belong
                            ViewCompat.animate(v)
                                    .setListener(null)
                                    .rotation(0f)
                                    .translationX(0f)
                                    .translationY(0f)
                                    .start()
                        }
                    }
                }

                return true
            }
        })

    }

}