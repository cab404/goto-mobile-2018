package com.cab404.diligence

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

private const val REQ_PHOTO = 778

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.vRoot, CardFragment())
                .commit()


        vTakeAPhoto.setOnClickListener {
            takeAPhoto()
        }

    }


    private fun takeAPhoto() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, REQ_PHOTO)
    }


    inline fun <reified V> V.log(text: Any) {
        Log.v(V::class.java.simpleName, text.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data!!
        if (requestCode == 778) {
            log(data.data)
        }
    }

}