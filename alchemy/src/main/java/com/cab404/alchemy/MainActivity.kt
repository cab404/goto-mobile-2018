package com.cab404.alchemy

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    class Element(
            val view: View,
            val name: String
    ) {

        fun overlaps(second: Element): Boolean {
            val rectA = Rect(second.view.left, second.view.top, second.view.right, second.view.bottom)
            val rectB = Rect(view.left, view.top, view.right, view.bottom)
            return rectA.intersect(rectB)
        }

        val x get() = view.left
        val y get() = view.top

    }


    val elements = mutableListOf<Element>()


    fun addElement(name: String, x: Int = 0, y: Int = 0): Element {
        val textView = TextView(this)
        discovered += name

//        elements.forEach { it.updateCoordinates() }

        with(textView) {
            text = "[${name}]"
            setTextColor(colors[name] ?: Color.DKGRAY)
            layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                //                gravity = Gravity.CENTER
                leftMargin = x
                topMargin = y
            }

            vRoot.addView(this)
            scaleX = 0f
            scaleY = 0f
            ViewCompat.animate(this)
                    .scaleX(1f)
                    .scaleY(1f)
                    .start()
        }
        makeMovable(textView)

//        textView.post { elements.forEach { it.restoreCoordinates() } }

        val element = Element(textView, name)
        elements += element
        return element
    }

    fun deleteElement(element: Element) {
        elements -= element
        ViewCompat
                .animate(element.view)
                .scaleX(0f)
                .scaleY(0f)
                .setListener(object : ViewPropertyAnimatorListenerAdapter() {
                    override fun onAnimationEnd(view: View?) {
                        vRoot.removeView(view)
                    }
                })
                .start()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v("main", "create!")

        // Создаем и выставляем layout в активити
        setContentView(R.layout.activity_main)

        // Добавляем фрагмент в лейаут
        val fragment = SelectElementFragment()
        fragment.listener = { name ->
            addElement(name)
        }
        supportFragmentManager
                .beginTransaction()
                .add(
                        R.id.vRoot, // Куда добавляем фрагмент
                        fragment, // Создаем фрагмент для добавления
                        "selector" // Тег, чтобы позже по нему можно было достать фрагмент
                )
                .commit()

        vAddElement.setOnClickListener {
            // Находим по нашему тегу selector
            /*
            * Expected: Fragment
            * Found: Fragment?
            */
            val selectorFragment = supportFragmentManager.findFragmentByTag("selector")
            // Если скрыто, то
            if (selectorFragment.isHidden) {
                supportFragmentManager.beginTransaction()
                        // показать
                        .show(selectorFragment)
                        .commit()
            } else {
                // иначе
                supportFragmentManager.beginTransaction()
                        // скрыть
                        .hide(selectorFragment)
                        .commit()
            }
        }

        addElement("Вода")
        addElement("Огонь")


    }


    // Вызывается при появлении activity на экране
    override fun onStart() {
        super.onStart()
        Log.v("main", "start!")
    }

    // Вызывается при исчезновении activity с экрана
    override fun onStop() {
        super.onStop()
        Log.v("main", "stop!")
    }

    // Вызывается, когда пользователь не может нажимать на элементы активити
    override fun onPause() {
        super.onPause()
        Log.v("main", "pause!")
    }

    // Вызывается, когда пользователь снова может нажимать на элементы активити
    override fun onResume() {
        super.onResume()
        Log.v("main", "resume!")
    }

    // Вызывается, когда активити мертва
    override fun onDestroy() {
        super.onDestroy()
        Log.v("main", "destroy!")
    }

    private fun makeMovable(view: View) {
        var sx = 0f
        var sy = 0f
        view.setOnTouchListener { view, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    sx = event.rawX
                    sy = event.rawY
                }
                MotionEvent.ACTION_MOVE -> {

//                    view.translationX += ((event.rawX - sx))
//                    view.translationY += ((event.rawY - sy))
                    with(view.layoutParams as FrameLayout.LayoutParams) {
                        leftMargin += (event.rawX - sx).toInt()
                        topMargin += (event.rawY - sy).toInt()
                        view.requestLayout()
                    }

                    sx = event.rawX
                    sy = event.rawY
                }
                MotionEvent.ACTION_UP -> {
                    Log.v("coord", "${view.x}:${view.y}")

                    val toDelete = mutableListOf<Element>()
                    val el1 = elements.first { it.view == view }

                    for (el2 in elements.filter { it != el1 }) {

                        if (el1.overlaps(el2)) {
                            Toast.makeText(
                                    this,
                                    "Overlap! ${el1.name} on ${el2.name}",
                                    Toast.LENGTH_SHORT
                            ).show()

                            if ((el1.name to el2.name) in pairs || (el2.name to el1.name) in pairs) {
                                val newName = pairs[(el1.name to el2.name)]
                                        ?: pairs[(el2.name to el1.name)]!!
                                addElement(
                                        newName,
                                        (el1.x + el2.x) / 2,
                                        (el1.y + el2.y) / 2
                                )
                                toDelete += el1
                                toDelete += el2
                            }

                        }
                    }

                    toDelete.forEach {
                        deleteElement(it)
                    }

                }
            }
            true
        }
    }

}