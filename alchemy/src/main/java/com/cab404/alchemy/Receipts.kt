package com.cab404.alchemy

import android.graphics.Color


// Цвета для вещей
val colors = mapOf(
        "Огонь" to Color.RED,
        "Вода" to Color.BLUE,
        "Земля" to Color.BLACK,
        "Воздух" to Color.CYAN,
        "Лава" to Color.RED,
        "Пар" to Color.LTGRAY
)

// Рецепты
val pairs = mapOf(
        ("Вода" to "Огонь") to "Пар",
        ("Земля" to "Огонь") to "Лава"
)


// Элементы,которые мы нашли
val discovered = mutableSetOf<String>(
        "Огонь",
        "Вода",
        "Земля",
        "Воздух"
)