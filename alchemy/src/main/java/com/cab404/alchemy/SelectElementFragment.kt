package com.cab404.alchemy

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_select_element.*

class SelectElementFragment : Fragment() {

    var listener: (String) -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_element, container, false)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)

        val discoveredList = discovered.toList()
        vList.adapter = ArrayAdapter<String>(context, R.layout.item, R.id.vTitle, discoveredList)
        vList.setOnItemClickListener { parent, view, position, id ->
                listener(discoveredList[position])
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


}