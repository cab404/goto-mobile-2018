package com.cab404.gotomobile

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_message.view.*

class MessageViewHolder(view: View): RecyclerView.ViewHolder(view){

}


val MESSAGE_DIFF: DiffUtil.ItemCallback<Message> = object : DiffUtil.ItemCallback<Message>() {
    override fun areItemsTheSame(oldItem: Message?, newItem: Message?): Boolean {
        return oldItem?.id == newItem?.id
    }

    override fun areContentsTheSame(oldItem: Message?, newItem: Message?): Boolean {
        return oldItem?.text == newItem?.text
    }
}

class MessageAdapter: ListAdapter<Message, MessageViewHolder>(MESSAGE_DIFF) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
        return MessageViewHolder(view)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val item = getItem(position)!!
        holder.itemView?.also {
            it.vText.text = item.text

        }
    }

}