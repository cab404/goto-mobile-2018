package com.cab404.gotomobile

class Message(
        val id: String,
        val from: String,
        val text: String
)
