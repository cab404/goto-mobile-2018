package com.cab404.thetrial

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val list = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = ArrayAdapter<String>(this, R.layout.item, R.id.text, list)
        vList.adapter = adapter

        var id = 0

        vAdd.setOnClickListener {
            list += vTextAdd.text.toString()
            id++
            adapter.notifyDataSetChanged()
        }

        vList.setOnItemClickListener { parent, view, position, id ->
            list.removeAt(position)
            adapter.notifyDataSetChanged()
        }

    }

}